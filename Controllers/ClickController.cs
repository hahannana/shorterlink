using System.Collections.Generic;
using HashidsNet;
using Microsoft.AspNetCore.Mvc;
using shorterLink.Interfaces;
using shorterLink.Models;
using System.Linq;

namespace shorterLink.Controllers
{
    public class ClickController : Controller
    {
        private ILinksRepository _repository;

        public ClickController(ILinksRepository linksRepository)
        {
            _repository = linksRepository;
        }

        [HttpGet]
        [Route("/{id}")]
        public IActionResult Index(string id)
        {
            var link = _repository.GetLinkByShortcut(id);
            if (link == null)
            {
                var links = _repository.GetLinks();
                ViewBag.Message = "Link not found. Add it if you like.";
                return View("../Link/Index", links);
            }
            else
            {
                return Redirect(link.YourLink);
            }
        }
    }
}