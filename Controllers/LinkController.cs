using System.Collections.Generic;
using HashidsNet;
using Microsoft.AspNetCore.Mvc;
using shorterLink.Interfaces;
using shorterLink.Models;
using System.Linq;

namespace shorterLink.Controllers
{
    public class LinkController : Controller
    {
        private ILinksRepository _repository;

        public LinkController(ILinksRepository linksRepository)
        {
            _repository = linksRepository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var links = _repository.GetLinks();
            return View(links);
        }

        [HttpPost]
        public IActionResult Create(Link link)
        {
            if (ModelState.IsValid)
            {
                link.Shortcut = ShortenLink(link);
                _repository.AddLink(link);
            }
            else
            {
                ViewBag.Message = "This is not valid url. You have to start with http:// or https://";
            }
            var links = _repository.GetLinks();
            return View("Index", links);
        }

        [HttpGet]
        public IActionResult Delete(Link link)
        {
            _repository.Delete(link);
            return Redirect("Index");
        }

        private string ShortenLink(Link link)
        {
            var charArray = link.YourLink.ToCharArray();
            var intArray = new int[charArray.Length];

            for (var i = 0; i < charArray.Length; i++)
            {
                intArray[i] = charArray[i];
            }

            var hashids = new Hashids("this is my salt", 8);
            var result = hashids.Encode(intArray.Sum());
            return result;
        }
    }
}