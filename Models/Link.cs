using System.ComponentModel.DataAnnotations;

namespace shorterLink.Models
{
    public class Link
    {
        [Display(Name="Your link")]
        [RegularExpression("^http(s)?://([\\w-]+.)+[\\w-]+(/[\\w- ./?%&=])?$")]
        public string YourLink { get; set; }

        [Display(Name="Shortcut")]
        public string Shortcut {get; set; }
    }
}