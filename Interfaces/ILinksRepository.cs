 using System.Collections.Generic;
 using shorterLink.Models;
 
 namespace shorterLink.Interfaces
 {
    public interface ILinksRepository
     {
          List<Link> GetLinks();
          void AddLink(Link link);
          Link GetLinkByShortcut(string shortcut);
          void Delete(Link link);
     }
 }