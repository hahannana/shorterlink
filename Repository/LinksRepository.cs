using System.Collections.Generic;
using System.Linq;
using shorterLink.Interfaces;
using shorterLink.Models;

namespace shorterLink.Repository
{
    public class LinksRepository : ILinksRepository
    {
        public static List<Link> _links = new List<Link>
    {
             };


        public void AddLink(Link link)
        {

            _links.Add(link);
        }

        public List<Link> GetLinks()
        {
            return _links;
        }

        public Link GetLinkByShortcut(string shortcut)
        {
            return _links.SingleOrDefault(l => l.Shortcut == shortcut);
        }

        public void Delete(Link link)
        {
            var linkToDelete = _links
            .SingleOrDefault(element => element.YourLink == link.YourLink);
            _links.Remove(linkToDelete);
        }
    }
}